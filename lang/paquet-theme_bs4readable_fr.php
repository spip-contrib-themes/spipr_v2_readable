<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4readable
// Langue: fr
// Date: 26-03-2020 11:53:13
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4readable_description' => 'Optimized for legibility',
	'theme_bs4readable_slogan' => 'Optimized for legibility',
);
?>